# Documentation

Welcome to SoCraTes!

Thank you for taking interest in our software project.

This is the central place to provide documentation, but also the main issue tracker for all the individual repos. It's a work in progress, so please don't be mad if you can't find whatever you are looking for - we are doing our best to keep things up to date. 
We'll be happy to answer your questions, if you send an email to *info (at) socrates-conference.de*, or in the #neogora channel on the SoCraTes Slack.

## Contributing

First of all, thank you for taking the time to contribute to the web application and SoCraTes conference.

What follows is a set of guidelines for contributing. Those are not rules so use your best judgement.

Also, feel free to propose changes to this document too.

### Application

The application is split into repositories
- [Backend](https://gitlab.com/socrates-conference/socrates-server), built using Node.js and Flow
- [Static Client](https://gitlab.com/socrates-conference/socrates-client), built using React / Redux and Flow
- [Attendee Client](https://gitlab.com/socrates-conference/socrates-attendee-client), built using React / Redux and Flow
- [Admin Client](https://gitlab.com/socrates-conference/socrates-admin-client), built using React / Redux and Flow

CI automatically deploys

| Branch  | Environment | Address                                                                           |
|---------|-------------|-----------------------------------------------------------------------------------|
| develop | staging     | [click open](https://gitlab.com/socrates-conference/socrates-client/environments) |
| master  | production  | https://socrates-conference.de                                                    |

Staging database:
- address: 18.197.143.144:3333
- database: socrates_db
- user: socrates
- password: eL,jW+6le7Q0mvR,

You can access it from a terminal with
```
mysql --host=18.197.143.144 --port=3333 --user=socrates --password="eL,jW+6le7Q0mvR," socrates_db
```

The application uses Rancher on top of Docker. The storage (database) is a Docker persistent volume hosted on AWS EBS. The development host and production hosts are AWS EC2 containers.

In Rancher we can setup environments (e.g. dev, prod) and assign services to them (eg socrates-client). Services that share a common configuration form a Deployment Unit. In particular, Docker Compose + Rancher Compose = Deployment Unit.

A service can be run by one or many containers. Each container is scheduled on to a host by Rancher depending on resources available. At the moment we have one host per environment (i.e. demo and prod), thus all services go there.

In case of low disk space
- purge Gitlab runners caches (buttons are available in the repositories)
- remove old docker images on the host with `sudo docker image prune --all`

Rancher url http://18.197.143.144:8080

We use Instana for monitoring: https://socrates-socrates.instana.io. Its alerts go to #neogora-alerts in Slack.

### Design

During SoCraTes 2017 a group of brave people did an Event Storming for the app. Review it before implementing a new feature:
- [Big Picture](./event_storming/big_picture.pdf)
- [Aggregates](./event_storming/aggregates.pdf)

### Requirements

- NodeJS v8.x (If you're on Linux/Mac, We strongly recommend using [NVM](https://github.com/creationix/nvm) to manage your node installations. There also is an [NVM for Windows](https://github.com/coreybutler/nvm-windows), but that's a different project - no guarantees. )
- [Yarn](https://yarnpkg.com/) (`npm install -g yarn`)
- `npm install -g flow-typed && flow-typed install jest@21.x.x` (should match jest version used in the project)

### Development

- `yarn` to install the app dependencies
- `yarn start` to run the application
- `yarn test:unit` or `yarn test:unit -t NAME` to run unit tests
- `yarn test:cucumber` to run Cucumber tests
- `yarn precommit` to run all tests and checks (check `package.json` to see how to run a subset of them)

### How to contribute

- Report bugs: open an issue with a clear description of the problem and
  how to reproduce (sometimes a [gif](https://giphy.com/) is worth a thousand words)
- Participate to discussions in #neogora on [Slack](http://socrates-conference.slack.com/)
- Participate to discussions in the issues
- Review non wip Merge Requests
- Create a Merge Request

### Merge Request process

#### As an author

- Join the [Gitlab group](https://gitlab.com/socrates-conference/), an admin will accept the request asap; this is needed because the CI runners are restricted to the project
- Branch out of `develop`
- Create a WIP Merge Request (add "WIP:" at the beginning of the title) with `develop` as target branch; we always merge first to `develop` to be able to test in the staging environment
- Explain as clearly as possible what the Merge Request does (consider adding screenshots or gifs)
- Write tests and code to implement the feature; try to keep the Merge Request as small as possible (~200 LOCs would be great): the smaller the Merge Request the easier to write for the author and review for the reviewer. If needed, split the feature into multiple Merge Requests.
- When ready and `yarn precommit` is happy, remove the WIP status and ask somebody to review and merge

#### As a reviewer

- Comment as clearly as possible (consider adding snippets of code)
- Check `yarn precommit` before merging. This is because in feature branches we run a subset of `yarn precommit` to save CI runner time. At the same time, on `develop` and `master` we run `yarn precommit` entirely
- After merging a Merge Request, close all related issues
- Check that the build is green on CI

### Issues

We keep all issues in [Trello](https://trello.com/b/ynXgtxl0/socrates-neogora). Please use the right label(s) to specify to which repo the ticket refers. Also, whenever working on an issue, assign yourself and move it to doing.

Please consider splitting tickets into smaller one whenever possible!

"good-first-issue" are a perfect starting point for new contributors.


## Repos
### [socrates-server](https://gitlab.com/socrates-conference/socrates-server)

#### Setting up the database

We use db-migrate for database migrations. Check the [docs](https://db-migrate.readthedocs.io/en/latest/) to learn more.

```bash
npm install -g db-migrate

# run mysql on :3306
# create a "socrates_db" database

export DB_ENV=local
export DB_USER="..."
export DB_PASS="..."
db-migrate up --env=$DB_ENV
# or
DB_USER="..." DB_PASS="..." db-migrate up --env=local
```

#### Modifying the database

```bash
db-migrate create my-migration-name # create migration files
# add to the UP file (eg `ALTER TABLE`, `CREATE TABLE`).
# add to the DOWN file (eg `DROP TABLE` if in the UP you added `CREATE TABLE`).
db-migrate up --env=local
```

#### Run production build

```bash
yarn build

# run mysql on :3306
# create a "socrates_db" database

export DB_ENV=local
export DB_USER="..."
export DB_PASS="..."

yarn serve
```

### [server-commons](https://gitlab.com/socrates-conference/server-commons)

Items used by all socrates server projects:

- App class: 
    Spin up and shutdown express server, connect Middlewares.
- Middlewares:
    - DefaultHeaders (CORS support, XSS prevention)
    - BodyParsers
    - Health check
    - AuthToken parsers
- Eventbus (with optional Kafka)
- Generic ConfigType

### [hello-socrates-server](https://gitlab.com/socrates-conference/hello-socrates-server)

The server for our little Hello World project consists of three layers:
1. The REST endpoints, implemented using an [expressjs](http://expressjs.com) middleware
2. A domain core, consisting of a single Aggregate ('Messages').
3. A repository, implemented in-memory (a simple array of messages).

#### Run the tests

```
yarn test
```

#### Run the local server

```
yarn start
```

#### Run production build

```
yarn build
yarn serve
```

## Admin guide (for organizers)

Visit the [admin dashboard](https://socrates-conference.de/admin).

### Remove confirmed participant

* click on participants
* find the participant to remove in the list
* click on the red bin icon and confirm
* the application sends an email to the participant automatically (https://gitlab.com/socrates-conference/socrates-server/tree/master/email)

### Run the lottery

* click on lottery
* free is the spots that will be assigned
* click on generate draw distribution
* click on distribution health check
* click on register drawn applications
* the application sends an email to the drawn participants automatically (https://gitlab.com/socrates-conference/socrates-server/tree/master/email)

Reserved rooms are not assigned to any applicants when running the lottery. In case you want to change the number of reserved rooms edit to code in https://gitlab.com/socrates-conference/socrates-server/blob/master/src/domain/conferenceData.js and deploy to production.

### Change room type for applicant

* click on applicants
* find the applicant in the list
* click on the blue door icon
* change room types (since the person was not yet extracted in the lottery more than one room type can be selected)

### Change room type for participant

* click on participants
* find the participant in the list
* click on the blue door icon
* change room types (since the person was already extracted in the lottery only one room type can be selected)

### Send email reminders to candidate participant who still have to confirm

* click on participants
* deadline is set to 1 month after the person was drawn in the lottery, last remainder is the time when the last remainder email has been sent
* click on the yellow @ icon
* depending on the amount of remainders already sent the participant will receive a different email (rules https://gitlab.com/socrates-conference/socrates-server/blob/master/src/domain/notification.js#L140) (emails  (https://gitlab.com/socrates-conference/socrates-server/tree/master/email); when the counter is at two the email is sent to registration@socrates-conference.de instead as a remainder to remove the participant

### Summary of tshirt sizes

* connect to db `mysql --host=IP --port=3333 --user=USER --password="PASSWORD" DATABASE_NAME`
* `use socrates_db;`
* `select COUNT(*), tshirt from participants group by tshirt;`
* Check the value of tshirt in the [socrates-attendee-client](https://gitlab.com/socrates-conference/socrates-attendee-client/blob/master/src/view/profile/Swag.jsx#L25)

## Check Server Logs

### Staging

* go to http://18.197.143.144:8080/env/1a5/apps/stacks/1st14/services/1s36/containers
* click on the three vertical dots icon
* click "View Logs"

### Production

* go to http://18.197.143.144:8080/env/1a7/apps/stacks/1st19/services/1s94/containers
* click on the three vertical dots icon
* click "View Logs"
